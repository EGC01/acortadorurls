<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\registrourl;

class RegistrarUrlController extends Controller
{



    public function create()
    {
        return view('create');
    }

    //Genera El url de solicitado
    public function generarURL(REQUEST $request)
    {

        $fullUrl = $request->url;
        $generatorstring = substr(md5(time()), 0, 5);
        $smallUrl = $generatorstring;

        if (!isset($fullUrl)) {
            return "La URL esta vacia";
        }

        //Valida que exista el egistro en la base de datos
        $existURL = registrourl::select('url_largo', 'url_corto')
        ->where('url_largo', $fullUrl)
        ->get();

        if (count($existURL) == 0) {
            $urlValida = false;

            while ($urlValida === true) {
                $validacion = registrourl::select()->where('url_corto', $generatorstring);

                if (count($validacion) == 0) {
                    $urlValida = true;
                } else {
                    $generatorstring = substr(md5(time()), 0, 5);
                    $smallUrl = $generatorstring;

                    $urlValida = false;
                }
            }

            $url = new registrourl();
            $url->url_corto = $smallUrl;
            $url->url_largo = $fullUrl;
            $url->save();

            return "Su URL ES" . " " . substr($request->fullUrl(), 0, -10) . "" . $smallUrl;
        }
            return "Su URL ES" . " " . substr($request->fullUrl(), 0, -10) . "" . $existURL[0]->url_corto;
    }

    public function buscar(Request $request, $id)
    {
        $urlValida = registrourl::select('url_largo')->where('url_corto', $id)->get();
        return redirect()->away($urlValida[0]->url_largo);
    }
}
