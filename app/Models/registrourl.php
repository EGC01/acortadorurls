<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class registrourl extends Model
{
    use HasFactory;

    protected $table = 'registrourl';
    public $timestamps = false;
    protected $fillable=[
        'url_corto',
        'url_largo'
    ];
}
